#include <stdint.h>
#include <stdlib.h>

#ifndef CIR_BUF_H
#define CIR_BUF_H

struct cbuf;

struct cbuf * cbuf_create(void);
/* 0 on success, -1 on fail */
int cbuf_writebyte(struct cbuf *cb, uint8_t byte);
/* 0 on success, -1 on fail */
int cbuf_readbyte(struct cbuf *cb, uint8_t *byte);
/* len on success, -1 on fail */
int cbuf_writebytes(struct cbuf *cb, const uint8_t * bytes, int len);

/* len on success, -1 on fail */
int cbuf_readbytes(struct cbuf *cb, void *bytes, int len);
int cbuf_bytesfree(struct cbuf *cb);
int cbuf_bytesinuse(struct cbuf *cb);

#endif
