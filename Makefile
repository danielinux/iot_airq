CROSS_COMPILE:=arm-none-eabi-
CC:=$(CROSS_COMPILE)gcc
LD:=$(CROSS_COMPILE)gcc
OBJS:=startup.o main.o timer.o system.o rtc.o cbuf.o newlib.o
UCMX=unicore-mx/lib/libucmx_stm32f4.a

LSCRIPT:=target.ld

OBJCOPY:=$(CROSS_COMPILE)objcopy


CFLAGS:=-mcpu=cortex-m3 -mthumb -g -ggdb -Wall -Wno-main -Wstack-usage=200 -ffreestanding -Wno-unused -nostartfiles -nostdlib
LDFLAGS:=-T $(LSCRIPT) -Wl,-gc-sections -Wl,-Map=image.map -nostartfiles -mcpu=cortex-m3 -mthumb
CFLAGS+=-Iunicore-mx/include/ -DSTM32F4

#all: image.bin

image.bin: image.elf
	$(OBJCOPY) -O binary $^ $@

$(UCMX):
	make -C unicore-mx TARGETS=stm32/f4 FP_FLAGS="-mfloat-abi=soft" PREFIX=arm-frosted-eabi

image.elf: $(UCMX) $(OBJS) $(LSCRIPT)
	$(LD) $(LDFLAGS) $(OBJS) $(UCMX) -o $@
	
startup.o: startup.c

main.o: main.c

clean:
	make -C unicore-mx clean
	rm -f image.bin image.elf *.o image.map $(UCMX)
