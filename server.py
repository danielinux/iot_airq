#!/usr/bin/python

# Loosely Based on a fork from
# https://github.com/aqicn/sds-sensor-reader
# Credits: Matjaz Rihtar, Matej Kovacic

import os
import sys
import time
import socket

# Reopen sys.stdout with buffer size 0 (unbuffered)
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

class SDS021Reader:

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.s.bind(("0.0.0.0",7777))

    def readValue( self ):
        step = 0
        while True: 
            buf = self.s.recv(10)
            if ord(buf[0]) != 170:
                continue
            if ord(buf[1]) != 192:
                continue
            pm25 = (ord(buf[3])*256 + ord(buf[2])) / 10.0
            pm10 = (ord(buf[5])*256 + ord(buf[4])) / 10.0
            return [pm25,pm10]

    def read( self ):
        species = [[],[]]

        while 1:
            try:
                values = self.readValue()
                species[0].append(values[0])
                species[1].append(values[1])
                print("PM2.5: {}, PM10: {}".format(values[0], values[1]))
                time.sleep(1)  # wait for one second
            except KeyboardInterrupt:
                print("Quit!")
                sys.exit()
            except:
                e = sys.exc_info()[0]
                print("Can not read sensor data! Error description: " + str(e))

def loop():
    print("Starting reading dust sensor on UDP port 7777") 
    reader = SDS021Reader() 
    while 1:
        reader.read()
loop()
