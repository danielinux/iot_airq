/*  
 *      This is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License version 2, as 
 *      published by the Free Software Foundation.
 *
 *      frosted is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with frosted.  If not, see <http://www.gnu.org/licenses/>.
 *
 *      Author: Daniele Lacamera
 *      
 *
 */  

#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/usart.h>
#include <unicore-mx/cm3/nvic.h>

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "system.h"
#include "timer.h"
#include "cbuf.h"

#define WIFICTL GPIO14 // A
#define LED0CTL GPIO13 // A
#define LED1CTL GPIO15 // A
#define AIRQCTL GPIO4  // B

#define USART_BUF 128
#define SAMPLES 10
#define SLEEPTIME


struct cbuf *inbuf[3];

volatile int timer_elapsed = 0;
volatile uint32_t tim2_ticks = 0;
volatile uint32_t cpu_freq = 48000000;
volatile int powersave = 1;

extern void rtc_init(void);
extern void rtc_start(void);

struct __attribute__((packed)) pm25_data {
    uint8_t hdr;
    uint8_t cmd;
    uint16_t pm25;
    uint16_t pm10;
    uint16_t id;
    uint8_t chk;
    uint8_t tail;
};

static struct pm25_data sensor_data;

static void sendwifi(void *_buf, int len)
{
    int i;
    uint8_t c = (uint8_t)len;
    uint8_t *buf = (uint8_t *)_buf;
    char pad[3] = "PAD";
    for (i = 0; i < (len + 4); i++) {
        if (i < 3) {
            c = pad[i];
            printf("Send preamble %02x\r\n", c);
            usart_send_blocking(USART6, (uint16_t)c);
        } else if (i == 3) {
            c = (uint8_t)len + 4;
            printf("Send len %02x\r\n", c);
            usart_send_blocking(USART6, (uint16_t)c);
        } else {
            printf("Send char %02x\r\n", buf[i - 4]);
            usart_send_blocking(USART6, (uint16_t)(buf[i - 4]));
        }
    }
}

static void led_setup(void) {
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);
    /* Wifi switch + 2 leds */
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO15 | GPIO14 | GPIO13 );
    gpio_set_output_options(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, GPIO15 | GPIO14 | GPIO13);
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO4);
    gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, GPIO4);
    gpio_clear(GPIOA, GPIO15 | GPIO14 | GPIO13);
    gpio_clear(GPIOB, GPIO4);
    gpio_clear(GPIOA, LED0CTL);
}

static int uread(int n, uint8_t *rbuf, uint32_t len)
{
    uint8_t *buf = NULL;
    uint32_t clen = 0;
    int ret = 0;
    struct cbuf *cb = NULL;
    asm volatile("cpsid i");
    switch (n) {
        case USART1:
            clen = cbuf_bytesinuse(inbuf[0]);
            cb = inbuf[0];
            break;
        case USART2:
            clen = cbuf_bytesinuse(inbuf[1]);
            cb = inbuf[1];
            break;
        case USART6:
            clen = cbuf_bytesinuse(inbuf[2]);
            cb = inbuf[2];
            break;
        default:
            clen = 0;
            ret = -1;
    }

    if (clen > 0) {
        if (clen < len)
            len = clen;
        ret = cbuf_readbytes(cb, rbuf, len);
    }
    asm volatile("cpsie i");
    return ret;
}

void enter_lowpower_mode(void)
{
    uint32_t scr = 0;
    gpio_clear(GPIOA, GPIO15 | GPIO14 | GPIO13);
    gpio_clear(GPIOB, GPIO4);
    scr = SCB_SCR;
    scr &= ~SCB_SCR_SEVONPEND;
    scr |= SCB_SCR_SLEEPDEEP;
    scr &= ~SCB_SCR_SLEEPONEXIT;
    SCB_SCR = scr;
    POW_CR |= POW_CR_CWUF | POW_CR_FPDS | POW_CR_PDDS;
    POW_SCR |= POW_CR_CSBF;
}

static int sensor_parse(uint8_t *data, uint32_t len)
{
    int r;
    struct pm25_data *pm = (struct pm25_data *)data; 
    if (len < sizeof(struct pm25_data))
        return -1;
    printf("Acquiring...\r\n");
    if ((pm->hdr != 0xAA) || (pm->tail != 0xAB))
    {
        printf("Data hdr:tail %02x:%02x \r\n", pm->hdr, pm->tail );
        return -1;
    }
    printf("PM25: %hu PM10: %hu\r\n", pm->pm25, pm->pm10);
    memcpy(&sensor_data, pm, sizeof(struct pm25_data));
    return 0;
}

static void uart_init(void)
{
    const uint32_t usarts[3] = { USART1, USART2, USART6 };
    const uint32_t br[3] = { 115200, 9600, 38400};
    const uint32_t irqs[3] = { NVIC_USART1_IRQ, NVIC_USART2_IRQ, NVIC_USART6_IRQ };
    int i;
    rcc_periph_clock_enable(RCC_USART1);
    rcc_periph_clock_enable(RCC_USART2);
    rcc_periph_clock_enable(RCC_USART6);
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO2 | GPIO3);
    gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6 | GPIO7);
    gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6 | GPIO7);
    gpio_set_output_options(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO2);
    gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO6);
    gpio_set_output_options(GPIOC, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO6);
    gpio_set_af(GPIOA, GPIO_AF7, GPIO2 | GPIO3);
    gpio_set_af(GPIOB, GPIO_AF7, GPIO6 | GPIO7);
    gpio_set_af(GPIOC, GPIO_AF8, GPIO6 | GPIO7);
    for (i = 0 ; i < 3; i++) {
        usart_disable(usarts[i]);
        usart_set_baudrate(usarts[i], br[i]);
        usart_set_databits(usarts[i], 8);
        usart_set_parity(usarts[i], USART_PARITY_NONE);
        usart_set_stopbits(usarts[i], USART_STOPBITS_1);
        usart_set_mode(usarts[i], USART_MODE_TX_RX);
        usart_set_flow_control(usarts[i], USART_FLOWCONTROL_NONE);
        usart_enable_rx_interrupt(usarts[i]);
        nvic_enable_irq(irqs[i]);
        usart_enable(usarts[i]);
        inbuf[i] = cbuf_create();
    }
}

enum main_state  { 
    ST_START = 0,
    ST_SENSE,
    ST_XMIT,
    ST_SLEEP
};
static enum main_state main_state = ST_START;

static char wifibuf[128];

void main(void) {
    int sleep = 0;
    uint32_t timeout_counter = 0;
    uint32_t sensor_counter = 0;
    uint8_t sensor_rawdata[USART_BUF];
    int sensor_rawdata_size = 0;
    int n;
    uint8_t byte;
    rcc_clock_setup_hse_3v3(&rcc_hse_12mhz_3v3[RCC_CLOCK_3V3_48MHZ]);
    led_setup();
    rtc_init();
    uart_init();
    timer_init(cpu_freq, 1, 1000);
    printf("Awake!\r\n");
    while(1) {
        if (timer_elapsed) {
            WFE(); /* Consume timer event */
            gpio_toggle(GPIOA, LED1CTL);
            timeout_counter++;
            timer_elapsed = 0;
            printf("tick!\r\n");
        }
        switch (main_state) {
            case ST_START:
                gpio_set(GPIOB, AIRQCTL);
                main_state = ST_SENSE;
                break;
            case ST_SENSE:
                if (timeout_counter > 30) {
                    gpio_clear(GPIOB, AIRQCTL);
                    main_state = ST_START;
                    timeout_counter = 0;
                }
                sensor_rawdata_size = cbuf_bytesinuse(inbuf[1]);
                if (sensor_rawdata_size >= sizeof(struct pm25_data)) {
                    sensor_rawdata_size = uread(USART2, sensor_rawdata, USART_BUF);
                    timeout_counter = 0;
                    if (sensor_parse(sensor_rawdata, sensor_rawdata_size) == 0) {
                        sensor_counter++;
                    }
                    if (sensor_counter > SAMPLES) {
                        gpio_clear(GPIOB, AIRQCTL);
                        main_state = ST_XMIT;
                        gpio_set(GPIOA, WIFICTL);
                        timeout_counter = 0;
                    }
                }
                break;
            case ST_XMIT:
                if (timeout_counter > 40) {
                    gpio_clear(GPIOA, WIFICTL);
                    main_state = ST_START;
                    timeout_counter = 0;
                }
                /* Xmit after 8 second silence */
                if (timeout_counter > 8) {
                    sendwifi(&sensor_data, sizeof(sensor_data));
                    main_state = ST_SLEEP;
                    timeout_counter = 0;
                }
                /* Consume characters */
                memset(wifibuf, 0, 128);
                n = uread(USART6, (uint8_t *)wifibuf, 128);
                if (n > 0) { 
                    printf("%s", wifibuf);
                    timeout_counter = 0;
                }
                break;
            case ST_SLEEP:
                gpio_toggle(GPIOA, LED0CTL);
                if (timeout_counter > 5) {
                    gpio_clear(GPIOA, LED0CTL | LED1CTL | WIFICTL);
                    gpio_clear(GPIOB, AIRQCTL);
                    enter_lowpower_mode();
                    rtc_start();
                    WFE(); /* Never returns */
                }
                break;
        }
        WFI();
    }
}

#define TIM2_SR   (*(volatile uint32_t *)(TIM2_BASE + 0x10))
#define TIM_SR_UIF   (1 << 0)

void isr_tim2(void) {
    TIM2_SR &= ~TIM_SR_UIF;
    tim2_ticks++;
    timer_elapsed++;
}

void isr_usart1(void) {
    char c;
    gpio_toggle(GPIOA, LED0CTL);
    if (cbuf_bytesfree(inbuf[0]) == 0) {
        c = usart_recv(USART1);
        usart_send(USART1, c);
        return;
    }
    if (usart_is_recv_ready(USART1)) {
        c = usart_recv(USART1);
        usart_send(USART1, c);
        cbuf_writebyte(inbuf[0], c);
    }
    nvic_clear_pending_irq(NVIC_USART1_IRQ);
}

void isr_usart2(void) {
    nvic_clear_pending_irq(NVIC_USART2_IRQ);
    gpio_toggle(GPIOA, LED0CTL);
    if (cbuf_bytesfree(inbuf[1]) == 0) {
        usart_recv(USART2);
        return;
    }
    if (usart_is_recv_ready(USART2))
        cbuf_writebyte(inbuf[1], (uint8_t)usart_recv(USART2));
}

void isr_usart6(void) {
    char c;
    nvic_clear_pending_irq(NVIC_USART6_IRQ);
    gpio_toggle(GPIOA, LED0CTL);
    if (cbuf_bytesfree(inbuf[2]) == 0) {
        c = usart_recv(USART6);
        return;
    }
    if (usart_is_recv_ready(USART6)) {
        c = usart_recv(USART6);
        cbuf_writebyte(inbuf[2], c);
    }
}

int _write(void *r, uint8_t *text, int len)
{
    char *p = (char *)text;
    int i = 0;
    volatile uint32_t reg;
    while(*p && (i < len)) {
        usart_send_blocking(USART1, *(p++));
        i++;
    }
    return len;
}

